  echo > /lab4.txt
echo "1.    Загрузить операционную систему. ">> /lab4.txt
echo "2.    Войти в виртуальную машину, контейнер или на удалённый сервер приложений (IP адрес XX.XX.XX.254, пользователь lxc<NN>, пароль спросить у преподавателя). ">> /lab4.txt
echo "3.    Определить домашний (начальный) каталог текущего пользователя. ">> /lab4.txt
echo "current work dir: `pwd` ">> /lab4.txt
echo "4.    В домашнем каталоге пользователя создать каталог work. ">> /lab4.txt
  mkdir work
echo "5.    Сделать каталог work текущим каталогом. ">> /lab4.txt
  cd work
echo "6.    Создать протокол выполнения лабораторной работы lab4.txt в каталоге work. ">> /lab4.txt
echo "7.    Распаковать все файлы с заготовками и исходным кодом из архива /container/ABC-Linux/lab4/source.tar.bz2`` в каталог ``work. ">> /lab4.txt
  tar xvjf /source.tar.bz2
echo "8.    Поместить список файлов в каталоге work в подробном формате в протокол выполнения лабораторной работы. ">> /lab4.txt
  ls -la >> /lab4.txt
echo "9.    Исправить ошибки в заготовках исходных текстов программ. ">> /lab4.txt
  #patch -p1 < /lab4.patch
echo "10.    Поместить исправленные файлы в протокол выполнения лабораторной работы. ">> /lab4.txt
  cat /lab4.patch >> /lab4.txt
echo "11.    Оттранслировать программу и установить её в каталог bin каталога work. ">> /lab4.txt
  mv Makefile Makefile.old
  sed 's/        /	/' Makefile.old > Makefile
  make
  make install
echo "12.    Поместить список файлов в каталоге work/bin в подробном формате в протокол выполнения лабораторной работы. ">> /lab4.txt
  ls -la bin >> /lab4.txt
echo "13.    Очистить каталог work от вспомогательных файлов ">> /lab4.txt
  make clean
echo "14.    Поместить список файлов в каталоге work в подробном формате в протокол выполнения лабораторной работы. ">> /lab4.txt
  ls -la >> /lab4.txt
echo "15.    Запустить оттранслированную программу. ">> /lab4.txt
echo "16.    Поместить вывод программы в протокол выполнения лабораторной работы. ">> /lab4.txt
  ./bin/lab4 >> /lab4.txt
echo "17.    Представить результаты выполнения работы преподавателю. ">> /lab4.txt
  cat /lab4.txt

