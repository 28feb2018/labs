echo "1.    Запустить операционную систему." >> lab5.txt
echo "2.    Войти в виртуальную машину, контейнер или на удалённый сервер приложений (c IP адресом XX.XX.XX.254, пользователь lxc<NN>, пароль спросить у преподавателя)." >> lab5.txt
echo "3.    В домашнем каталоге пользователя создать каталог work." >> lab5.txt
echo "$ mkdir work" >> lab5.txt
mkdir work >> lab5.txt
echo "4.    Написать программу, которая сохраняет значения аргументов командной строки и параметров окружающей среды в файл lab5.txt." >> lab5.txt
echo "5.    Написать makefile, обеспечивающий трансляцию, установку, очистку и удаление программы (см. лаб. 4)" >> lab5.txt
echo "6.    Оттранслировать программу и установить её в каталог bin каталога work с помощью команды make." >> lab5.txt
echo "$ make install" >> lab5.txt
make install >> lab5.txt
echo "7.    Очистить каталог work от вспомогательных файлов с помощью команды make." >> lab5.txt
echo "$ make clean" >> lab5.txt
make clean >> lab5.txt
echo "8.    Запустить оттранслированную программу." >> lab5.txt
echo "$ ./bin/lab5 -f flagvalue example argument" >> lab5.txt
./bin/lab05 -f flagvalue example argument>> lab5.txt
echo "9.    Представить результаты выполнения работы преподавателю." >> lab5.txt
cat lab5.txt
