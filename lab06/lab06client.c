#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>

#define FTOKSEMPATH "/tmp/lab06sem" /*Путь к файлу, передаваемому ftok для набора семафоров */
#define FTOKSHMPATH "/tmp/lab06shm" /*Путь к файлу, передаваемому ftok для разделяемого сегмента */
#define FTOKID 1                    /*Идентификатор, передаваемый ftok */

#define NUMSEMS 2                   /* Число семафоров в наборе */
#define SIZEOFSHMSEG 512            /* Размер сегмента разделяемой памяти */

#define NUMMSG 5                    /* Число передаваемых сообщений */

int main(int argc, char *argv[])
{
	struct sembuf operations[3];
	void         *shm_address;
	int i, semid, shmid, ret_val;
	key_t semkey, shmkey;

	/*Создание IPC-ключей*/
	semkey = ftok(FTOKSEMPATH,FTOKID);
	if ( semkey == (key_t)-1 )
	{
		printf("Клиент: ошибка при выполнении %s\n","semkey = ftok(FTOKSEMPATH,FTOKID);");
		return -1;
	}
	shmkey = ftok(FTOKSHMPATH,FTOKID);
	if ( shmkey == (key_t)-1 )
	{
		printf("Клиент: ошибка при выполнении %s\n","shmkey = ftok(FTOKSHMPATH,FTOKID);");
		return -1;
	}

	/*Получение набора семафоров с помощью IPC-ключей*/
	semid = semget( semkey, NUMSEMS, 0666);
	if ( semid == -1 )
	{
		printf("Клиент: ошибка при выполнении %s\n","semid = semget( semkey, NUMSEMS, 0666);");
		return -1;
	}

	/*Получение сегмента разделяемой памяти*/
	shmid = shmget(shmkey, SIZEOFSHMSEG, 0666);
	if (shmid == -1)
	{
		printf("Клиент: ошибка при выполнении %s\n","shmid = shmget(shmkey, SIZEOFSHMSEG, 0666);");
		return -1;
	}

	/*Прикрепление сегмента разделяемой памяти, получение адреса*/
	shm_address = shmat(shmid, NULL, 0);
	if ( shm_address==NULL )
	{
		printf("Клиент: ошибка при выполнении %s\n","shm_address = shmat(shmid, NULL, 0);");
		return -1;
	}

	/*Цикл отправки сообщений. Выполняется NUMMSG раз*/
	for (i=0; i < NUMMSG; i++)
	{
		/* Клиент ожидает появления 0 на первом семафоре (сегмент разделяемой памяти свободен) */
		/* и 0 на первом семафоре (сегмент обработан сервером) */
		/* затем выставляет 1 на первом семафоре (сегмент занят) */
		/**/
		operations[0].sem_num = 0;
		operations[0].sem_op =  0;
		operations[0].sem_flg = 0;

		operations[1].sem_num = 1;
		operations[1].sem_op =  0;
		operations[1].sem_flg = 0;

		operations[2].sem_num = 0;
		operations[2].sem_op =  1;
		operations[2].sem_flg = 0;

		ret_val = semop( semid, operations, 3 );
		if (ret_val == -1)
		{
			printf("Клиент: ошибка при выполнении %s\n","ret_val = semop( semid, operations, 2 );");
			return -1;
		}

		snprintf( (char *) shm_address, SIZEOFSHMSEG, "Message from client with pid=%d", getpid() );
		usleep(200);
		/* Установить первый семафор в 0 (сегмент свободен), */
		/* второй семафор в 1 (сегмент изменен)              */
		operations[0].sem_num = 0;
		operations[0].sem_op =  -1;
		operations[0].sem_flg = 0;

		operations[1].sem_num = 1;
		operations[1].sem_op =  1;
		operations[1].sem_flg = 0;
		ret_val = semop( semid, operations, 2 );
		if (ret_val == -1)
		{
			printf("Клиент: ошибка при выполнении %s\n","ret_val = semop( semid, operations, 2 );");
			return -1;
		}
	}  /* Конец цикла отправки сообщений */
	/*Открепление сегмента разделяемой памяти.*/
	ret_val = shmdt(shm_address);
	if (ret_val==-1)
	{
		printf("Клиент: ошибка при выполнении %s\n","ret_val = shmdt(shm_address);");
		return -1;
	}

	return 0;
}
