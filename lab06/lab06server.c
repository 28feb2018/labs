#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>


#define FTOKSEMPATH "/tmp/lab06sem" /*Путь к файлу, передаваемому ftok для набора семафоров */
#define FTOKSHMPATH "/tmp/lab06shm" /*Путь к файлу, передаваемому ftok для разделяемого сегмента */
#define FTOKID 1                    /*Идентификатор, передаваемый ftok */

#define NUMSEMS 2                   /* Число семафоров в наборе */
#define SIZEOFSHMSEG 512            /* Размер сегмента разделяемой памяти */

#define NUMMSG 20                   /* Число принимаемых сообщений */


int main(int argc, char *argv[])
{
	int ret_val, rc, semid, shmid, i;
	key_t semkey, shmkey;
	void *shm_address;
	struct sembuf operations[2];
	struct shmid_ds shmid_struct;
	FILE *fp;

	/*Создание файлов для ftok*/
	fp=fopen(FTOKSEMPATH,"w");
	fclose(fp);
	fp=fopen(FTOKSHMPATH,"w");
	fclose(fp);

	/*Создание IPC-ключей*/
	semkey = ftok(FTOKSEMPATH,FTOKID);
	if ( semkey == (key_t)-1 )
	{
		printf("Сервер: ошибка при выполнении %s\n","semkey = ftok(FTOKSEMPATH,FTOKID);");
		return -1;
	}
	shmkey = ftok(FTOKSHMPATH,FTOKID);
	if ( shmkey == (key_t)-1 )
	{
		printf("Сервер: ошибка при выполнении %s\n","shmkey = ftok(FTOKSHMPATH,FTOKID);");
		return -1;
	}

	/*Создание набора семафоров с помощью IPC-ключей*/
	semid = semget( semkey, NUMSEMS, 0666 | IPC_CREAT | IPC_EXCL);
	if ( semid == -1 )
	{
		printf("Сервер: ошибка при выполнении %s\n","semid = semget( semkey, NUMSEMS, 0666 | IPC_CREAT | IPC_EXCL);");
		return -1;
	}

	/* В данной работе будет использоваться 2 семафора*/
	/* 1 в первом означает что облать разделяемой памяти используется*/
	/* 1 во втором, означает, что область разделяемой памяти изменена клиентом*/

	/*Инициализация семафоров*/
	ret_val = semctl( semid, 0, SETVAL, 0);
	if(ret_val == -1)
	{
		printf("Сервер: ошибка при выполнении %s\n","ret_val = semctl( semid, 0, SETVAL, 0);");
		return -1;
	}

	ret_val = semctl( semid, 1, SETVAL, 0);
	if(ret_val == -1)
	{
		printf("Сервер: ошибка при выполнении %s\n","ret_val = semctl( semid, 1, SETVAL, 0);");
		return -1;
	}

	/*Создание сегмента разделяемой памяти*/
	shmid = shmget(shmkey, SIZEOFSHMSEG, 0666 | IPC_CREAT | IPC_EXCL);
	if (shmid == -1)
	{
		printf("Сервер: ошибка при выполнении %s\n","shmid = shmget(shmkey, SIZEOFSHMSEG, 0666 | IPC_CREAT | IPC_EXCL);");
		return -1;
	}

	/*Прикрепление сегмента разделяемой памяти, получение адреса*/
	shm_address = shmat(shmid, NULL, 0);
	if ( shm_address==NULL )
	{
		printf("Сервер: ошибка при выполнении %s\n","shm_address = shmat(shmid, NULL, 0);");
		return -1;
	}
	printf("Сервер готов принимать сообщения от клиентов. Данный сервер настроен на прием %d сообщений\n", NUMMSG);

	/*Цикл обработки сообщений. Выполняется NUMMSG раз*/
	for (i=0; i < NUMMSG; i++)
	{
		/* Сервер ожидает появления 1 на втором семафоре (сегмент разделяемой памяти изменен клиентом) */
		/* затем выставляет 1 на первом семафоре (сегмент занят) */
		/**/
		operations[0].sem_num = 1;
		operations[0].sem_op = -1;
		operations[0].sem_flg = 0;

		operations[1].sem_num = 0;
		operations[1].sem_op =  1;
		operations[1].sem_flg = IPC_NOWAIT;

		ret_val = semop( semid, operations, 2 );
		if (ret_val == -1)
		{
			printf("Сервер: ошибка при выполнении %s\n","ret_val = semop( semid, operations, 2 );");
		}


		/*Обработать сообщение, полученное от клиента*/
		printf("Получено сообщение : \"%s\"\n", (char *) shm_address);

		/*Установить первый семафор в 0 (сегмент свободен)*/
		operations[0].sem_num = 0;
		operations[0].sem_op  = -1;
		operations[0].sem_flg = IPC_NOWAIT;

		ret_val = semop( semid, operations, 1 );
		if (ret_val == -1)
		{
			printf("Сервер: ошибка при выполнении %s\n","ret_val = semop( semid, operations, 1 );");
			return -1;
		}

	} /* Конец цикла обработки сообщений */

	/* Освобождние набора семафоров,            */
	/* открепление сегмента разделяемой памяти, */
	/* его освобождение.                        */
	ret_val = semctl( semid, 1, IPC_RMID );
	if (ret_val==-1)
	{
		printf("Сервер: ошибка при выполнении %s\n","ret_val = semctl( semid, 1, IPC_RMID );");
		return -1;
	}
	ret_val = shmdt(shm_address);
	if (ret_val==-1)
	{
		printf("Сервер: ошибка при выполнении %s\n","ret_val = shmdt(shm_address);");
		return -1;
	}
	ret_val = shmctl(shmid, IPC_RMID, &shmid_struct);
	if (ret_val==-1)
	{
		printf("Сервер: ошибка при выполнении %s\n","ret_val = shmctl(shmid, IPC_RMID, &shmid_struct);");
		return -1;
	}

	/*Удаление файлов для ftok*/
	unlink(FTOKSHMPATH);
	unlink(FTOKSEMPATH);
	return 0;
}
