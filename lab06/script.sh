echo "1.    Запустить операционную систему." > lab6.txt
echo "2.    Войти в виртуальную машину, контейнер или на удалённый сервер приложений (c IP адресом XX.XX.XX.254, пользователь lxc<NN>, пароль спросить у преподавателя)." >> lab6.txt
echo "3.    В домашнем каталоге пользователя создать каталог work." >> lab6.txt
echo "$ mkdir work" >> lab6.txt
mkdir work >> lab6.txt
echo "4.    Написать программу для обмена текстовыми сообщениями между процессами, с использованием механизма разделяемой памяти. Обеспечить синхронизацию обмена с помощью механизма семафоров." >> lab6.txt
echo "5.    Написать makefile, обеспечивающий трансляцию, установку, очистку и удаление программы (см. лаб. 4)" >> lab6.txt
echo "6.    Оттранслировать программу и установить её в каталог bin каталога work с помощью команды make." >> lab6.txt
echo "$ make install" >> lab6.txt
make install >> lab6.txt
echo "7.    Очистить каталог work от вспомогательных файлов с помощью команды make." >> lab6.txt
echo "$ make clean" >> lab6.txt
make clean >> lab6.txt
echo "8.    Запустить оттранслированную программу." >> lab6.txt
echo "$ ./bin/lab06server & ./bin/lab06client & ./bin/lab06client & ./bin/lab06client & ./bin/lab06client" >> lab6.txt
./bin/lab06server >> lab6.txt &
sleep 0.1 
./bin/lab06client >> lab6.txt & ./bin/lab06client >> lab6.txt & ./bin/lab06client >> lab6.txt & ./bin/lab06client >> lab6.txt
sleep 0.1
echo "9.    Представить результаты выполнения работы преподавателю." >> lab6.txt
cat lab6.txt
